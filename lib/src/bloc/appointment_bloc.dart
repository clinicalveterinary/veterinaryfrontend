import 'dart:async';

import 'package:clinical_veterinary/src/model/api_response_model.dart';
import 'package:clinical_veterinary/src/model/appointment_model.dart';
import 'package:clinical_veterinary/src/model/error_api_response_model.dart';
import 'package:clinical_veterinary/src/repository/generalveterinary_repository.dart';
import 'package:f_logs/model/flog/flog.dart';

class AppointmentBloc {
  final _repository = GeneralVeterinaryRepository();
  var _apiResponse = ApiResponse();
  final _appointmentListController =
      StreamController<List<Appointment>>.broadcast();
  final _appointmentController = StreamController<Appointment>.broadcast();

  List<Appointment> _initialList;

  Stream<List<Appointment>> get appointmentList =>
      _appointmentListController.stream.asBroadcastStream();

  Stream<Appointment> get appointment =>
      _appointmentController.stream.asBroadcastStream();

  ApiResponse get apiResponse => _apiResponse;

  AppointmentBloc();

  Future initializeData(String topic) async {
    _apiResponse = await _repository.getAllByTopicAppointment(topic);
    if (_apiResponse.statusResponse == 200) {
      _initialList = _apiResponse.payload;
      _appointmentListController.add(_initialList);
    } else {
      ErrorApiResponse error = _apiResponse.payload;
      FLog.error(text: error.message);
    }
  }

  void dispose() {
    _appointmentController.close();
    _appointmentListController.close();
  }
}
