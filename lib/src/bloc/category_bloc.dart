import 'dart:async';

import 'package:clinical_veterinary/src/model/api_response_model.dart';
import 'package:clinical_veterinary/src/model/category_model.dart';
import 'package:clinical_veterinary/src/model/error_api_response_model.dart';
import 'package:clinical_veterinary/src/repository/generalveterinary_repository.dart';
import 'package:clinical_veterinary/src/utils/Constants.dart';
import 'package:f_logs/model/flog/flog.dart';

class CategoryBloc {
  final _repository = GeneralVeterinaryRepository();
  var _apiResponse = ApiResponse();
  final _categoryListController = StreamController<List<Category>>.broadcast();
  final _categoryController = StreamController<Category>.broadcast();

  Stream<List<Category>> get categoryList =>
      _categoryListController.stream.asBroadcastStream();

  Stream<Category> get category =>
      _categoryController.stream.asBroadcastStream();

  ApiResponse get apiResponse => _apiResponse;

  CategoryBloc();

  Future getByName(String name) async {
    _apiResponse = await _repository.getCategoryByName(name);

    if (apiResponse.statusResponse == 200) {
      Category category = _apiResponse.payload;
      _categoryController.add(category);
    } else {
      ErrorApiResponse error = _apiResponse.payload;
      FLog.error(text: error.message);
    }
  }

  Future<ApiResponse> createCategory(Category category) async {
    _apiResponse = await _repository.insertCateogry(category);
    if (_apiResponse.statusResponse == 200) {
      _apiResponse.message = Constants.insertSuccess;
    } else {
      ErrorApiResponse error = _apiResponse.payload;
      FLog.error(text: error.message);
    }
    return _apiResponse;
  }

  Future<ApiResponse> updateCategory(Category category) async {
    _apiResponse = await _repository.updateCategory(category);
    if (_apiResponse.statusResponse == 200) {
      _apiResponse.message = Constants.updateSuccess;
    } else {
      ErrorApiResponse error = _apiResponse.payload;
      FLog.error(text: error.message);
    }
    return _apiResponse;
  }

  void dispose() {
    _categoryController.close();
    _categoryListController.close();
  }
}
