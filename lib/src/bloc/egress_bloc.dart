import 'dart:async';

import 'package:clinical_veterinary/src/model/api_response_model.dart';
import 'package:clinical_veterinary/src/model/egress_model.dart';
import 'package:clinical_veterinary/src/model/error_api_response_model.dart';
import 'package:clinical_veterinary/src/repository/generalveterinary_repository.dart';
import 'package:clinical_veterinary/src/utils/Constants.dart';
import 'package:f_logs/model/flog/flog.dart';

class EgressBloc {
  final _repository = GeneralVeterinaryRepository();
  var _apiResponse = ApiResponse();
  final _egressListController = StreamController<List<Egress>>.broadcast();
  final _egressController = StreamController<Egress>.broadcast();

  List<Egress> _initialList;

  Stream<List<Egress>> get egressList =>
      _egressListController.stream.asBroadcastStream();

  Stream<Egress> get egress => _egressController.stream.asBroadcastStream();

  ApiResponse get apiResponse => _apiResponse;

  EgressBloc();

  Future initializeData() async {
    _apiResponse = await _repository.getAllEgress();
    if (_apiResponse.statusResponse == 200) {
      _initialList = _apiResponse.payload;
      _egressListController.add(_initialList);
    } else {
      ErrorApiResponse error = _apiResponse.payload;
      FLog.error(text: error.message);
    }
  }

  Future getByPet(int idPet) async {
    _apiResponse = await _repository.getAllEgressByPet(idPet);

    if (_apiResponse.statusResponse == 200) {
      _initialList = _apiResponse.payload;
      _egressListController.add(_initialList);
    } else {
      ErrorApiResponse error = _apiResponse.payload;
      FLog.error(text: error.message);
    }
  }

  Future getByType(String type) async {
    _apiResponse = await _repository.getAllEgressByType(type);

    if (_apiResponse.statusResponse == 200) {
      _initialList = _apiResponse.payload;
      _egressListController.add(_initialList);
    } else {
      ErrorApiResponse error = _apiResponse.payload;
      FLog.error(text: error.message);
    }
  }

  Future<ApiResponse> createEgress(Egress egress) async {
    _apiResponse = await _repository.createEgress(egress);
    if (_apiResponse.statusResponse == 200) {
      _apiResponse.message = Constants.insertSuccess;
    } else {
      ErrorApiResponse error = _apiResponse.payload;
      FLog.error(text: error.message);
    }
    return _apiResponse;
  }

  Future<ApiResponse> updateEgress(Egress egress) async {
    _apiResponse = await _repository.updateEgress(egress);
    if (_apiResponse.statusResponse == 200) {
      _apiResponse.message = Constants.updateSuccess;
    } else {
      ErrorApiResponse error = _apiResponse.payload;
      FLog.error(text: error.message);
    }
    return _apiResponse;
  }

  void dispose() {
    _egressController.close();
    _egressListController.close();
  }
}
