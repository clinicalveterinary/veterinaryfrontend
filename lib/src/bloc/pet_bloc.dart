import 'dart:async';

import 'package:clinical_veterinary/src/model/api_response_model.dart';
import 'package:clinical_veterinary/src/model/error_api_response_model.dart';
import 'package:clinical_veterinary/src/model/pet_model.dart';
import 'package:clinical_veterinary/src/repository/generalveterinary_repository.dart';

class PetBloc {
  final _repository = GeneralVeterinaryRepository();
  var _apiResponse = ApiResponse();
  final _petListController = StreamController<List<Pet>>.broadcast();
  final _petController = StreamController<Pet>.broadcast();

  List<Pet> _initialList;

  Stream<List<Pet>> get petList =>
      _petListController.stream.asBroadcastStream();

  Stream<Pet> get pet => _petController.stream.asBroadcastStream();

  PetBloc();

  Future initializeData(String idUser) async {
    ApiResponse apiResponse;
    apiResponse = await _repository.getAllPetByUser(idUser);

    if (apiResponse.statusResponse == 200) {
      _initialList = apiResponse.payload;
      _petListController.add(_initialList);
    } else {
      ErrorApiResponse error = apiResponse.payload;
      _apiResponse = apiResponse;
      Flog.error(text: error.message);
    }
  }

  void disponse() {
    _petController.close();
    _petListController.close();
  }
}
