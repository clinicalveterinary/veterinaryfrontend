class Appointment {
  int idAppointment;
  DateTime dateTimeAppointment;
  bool stateAppointment;
  String topicAppointment;
  int idPet;

  Appointment({
    this.idAppointment,
    this.dateTimeAppointment,
    this.stateAppointment,
    this.topicAppointment,
    this.idPet
  });

  factory Appointment.fromJson(Map<String, dynamic> parsedJson){
   return Appointment(
     idAppointment: parsedJson['idAppointment'],
     dateTimeAppointment: parsedJson['dateTimeAppointment'],
     stateAppointment: parsedJson['stateAppointment'],
     topicAppointment: parsedJson['topicAppointment'],
     idPet: parsedJson['idPet']
   );
 }

 Map<String, dynamic> toJson() => {
   'idAppointment': idAppointment,
   'dateTimeAppointment': dateTimeAppointment,
   'stateAppointment': stateAppointment,
   'topicAppointment': topicAppointment,
   'idPet':idPet
 };

 Map<String, dynamic> toJsonRegistry() => {
   'dateTimeAppointment': dateTimeAppointment,
   'stateAppointment': stateAppointment,
   'topicAppointment': topicAppointment,
   'idPet':idPet
};

}