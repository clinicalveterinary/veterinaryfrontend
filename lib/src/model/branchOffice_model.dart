class BranchOffice {
  int idBranchOffice;
  int branchPhone;
  String address;
  String logo;
  int admin;
  int idVeterinaria;

  BranchOffice(
      {this.idBranchOffice,
      this.branchPhone,
      this.address,
      this.logo,
      this.admin,
      this.idVeterinaria});

  factory BranchOffice.fromJson(Map<String, dynamic> parsedJson) {
    return BranchOffice(
        idBranchOffice: parsedJson['idBranchOffice'],
        branchPhone: parsedJson['branchPhone'],
        address: parsedJson['address'],
        logo: parsedJson['logo'],
        admin: parsedJson['admin'],
        idVeterinaria: parsedJson['idVeteinary']);
  }

  Map<String, dynamic> toJson() => {
        'idBranchOffice': idBranchOffice,
        'branchPhone': branchPhone,
        'address': address,
        'logo': logo,
        'admin': admin,
        'idVeterinary': idVeterinaria
      };
  Map<String, dynamic> toJsonRegistry() => {
        'branchPhone': branchPhone,
        'address': address,
        'logo': logo,
        'admin': admin,
        'idVeterinary': idVeterinaria
      };
}
