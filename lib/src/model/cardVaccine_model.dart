class CardVaccine {
  int idCardVaccine;
  int idPet;
  
  CardVaccine(
    {this.idCardVaccine,
    this.idPet});

factory CardVaccine.fromJson(Map<String, dynamic> parsedJson) {
  return CardVaccine(
    idCardVaccine: parsedJson['idCardVaccine'],
    idPet: parsedJson['idPet']
  );
}

Map<String, dynamic> toJson() => {
  'idCardVaccine': idCardVaccine,
  'idPet': idPet
};

Map<String, dynamic> toJsonRegistry() => {
  'idPet': idPet
};

}