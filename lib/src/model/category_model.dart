
class Category{
  int id;
  String name;
  String status;
  String description;

  Category(
    {this.id,
    this.name,
    this.status,
    this.description
    }
  );

  factory Category.fromJson(Map<String, dynamic> parsedJson){
    return Category(
      id: parsedJson['id'], 
      description: parsedJson['description'], 
      name: parsedJson['name'], 
      status: parsedJson['status']);
  }

  Map<String, dynamic> toJson() =>{
    'id':id,
    'description':description,
    'name':name,
    'status':status
  };

    Map<String, dynamic> toJsonRegistry() =>{
    'description':description,
    'name':name,
    'status':status
  };
}