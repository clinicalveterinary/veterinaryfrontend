class ClinicalHistoryDetail {
  int idDetailHC;
  double temperature;
  double weight;
  double heartRate;
  double breathingFrequency;
  DateTime admissionDate;
  int idClinicalHistory;
  String feeding;
  DateTime dewormingDate;
  String habitat;
  double capillaryTime;
  String reproductiveStatus;
  String observations;
  int idCollaborator;

  ClinicalHistoryDetail(
      {this.idDetailHC,
      this.temperature,
      this.weight,
      this.heartRate,
      this.breathingFrequency,
      this.admissionDate,
      this.idClinicalHistory,
      this.feeding,
      this.dewormingDate,
      this.habitat,
      this.capillaryTime,
      this.reproductiveStatus,
      this.observations,
      this.idCollaborator});

  factory ClinicalHistoryDetail.fromJson(Map<String, dynamic> parsedJson) {
    return ClinicalHistoryDetail(
        idDetailHC: parsedJson['idDetailHC'],
        temperature: parsedJson['temperature'],
        weight: parsedJson['weight'],
        heartRate: parsedJson['heartRate'],
        breathingFrequency: parsedJson['breathingFrequency'],
        admissionDate: parsedJson['admissionDate'],
        idClinicalHistory: parsedJson['idClinicalHistory'],
        feeding: parsedJson['feeding'],
        dewormingDate: parsedJson['dewormingDate'],
        habitat: parsedJson['habitat'],
        capillaryTime: parsedJson['capillaryTime'],
        reproductiveStatus: parsedJson['reproductiveStatus'],
        observations: parsedJson['observations'],
        idCollaborator: parsedJson['idCollaborator']);
  }

  Map<String, dynamic> toJson() => {
        'idDetailHC': idDetailHC,
        'temperature': temperature,
        'weight': weight,
        'heartRate': heartRate,
        'breathingFrequency': breathingFrequency,
        'admissionDate': admissionDate,
        'idClinicalHistory': idClinicalHistory,
        'feeding': feeding,
        'dewormingDate': dewormingDate,
        'habitat': habitat,
        'capillaryTime': capillaryTime,
        'reproductiveStatus': reproductiveStatus,
        'observations': observations,
        'idCollaborator': idCollaborator
      };

  Map<String, dynamic> toJsonRegistry() => {
        'temperature': temperature,
        'weight': weight,
        'heartRate': heartRate,
        'breathingFrequency': breathingFrequency,
        'admissionDate': admissionDate,
        'idClinicalHistory': idClinicalHistory,
        'feeding': feeding,
        'dewormingDate': dewormingDate,
        'habitat': habitat,
        'capillaryTime': capillaryTime,
        'reproductiveStatus': reproductiveStatus,
        'observations': observations,
        'idCollaborator': idCollaborator
      };
}
