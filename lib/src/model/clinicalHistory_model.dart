class ClinicalHistory {
  int idClinicalHistory;
  int idPet;
  DateTime dateCreate;

  ClinicalHistory({this.idClinicalHistory, this.idPet, this.dateCreate});

  factory ClinicalHistory.fromJson(Map<String, dynamic> parsedJson) {
    return ClinicalHistory(
        idClinicalHistory: parsedJson['idClinicalHistory'],
        idPet: parsedJson['idPet'],
        dateCreate: parsedJson['dateCreate']);
  }

  Map<String, dynamic> toJson() => {
        'idClinicalHistory': idClinicalHistory,
        'idPet': idPet,
        'dateCreate': dateCreate
      };

  Map<String, dynamic> toJsonRegistry() =>
      {'idPet': idPet, 'dateCreate': dateCreate};
}
