class DetailInvoice {
  int idDetail;
  int quantity;
  int price;
  int idService;
  int idInvoice;
  int idColaborator;

  DetailInvoice(
      {this.idDetail,
      this.quantity,
      this.price,
      this.idService,
      this.idInvoice,
      this.idColaborator});

  factory DetailInvoice.fromJson(Map<String, dynamic> parsedJson) {
    return DetailInvoice(
        idDetail: parsedJson['idDetail'],
        quantity: parsedJson['quantity'],
        price: parsedJson['price'],
        idService: parsedJson['idService'],
        idInvoice: parsedJson['idInvoice'],
        idColaborator: parsedJson['idColaborator']);
  }

  Map<String, dynamic> toJson() => {
        'idDetail': idDetail,
        'quantity': quantity,
        'price': price,
        'idService': idService,
        'idInvoice': idInvoice,
        'idColaborator': idColaborator
      };

  Map<String, dynamic> toJsonRegistry() => {
        'idDetail': idDetail,
        'quantity': quantity,
        'price': price,
        'idService': idService,
        'idInvoice': idInvoice,
        'idColaborator': idColaborator
      };
}
