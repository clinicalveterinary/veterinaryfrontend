import 'dart:ffi';

class DetailVaccine{
 int idDetailVaccine;
 String nameVaccine;
 Float doseVaccine;
 DateTime dateVaccine;
 Float codeVaccine;
 int idCardVaccine;
 bool state;

 DetailVaccine({
   this.idDetailVaccine,
   this.nameVaccine,
   this.doseVaccine,
   this.dateVaccine,
   this.codeVaccine,
   this.idCardVaccine,
   this.state
 });

 factory DetailVaccine.fromJson(Map<String, dynamic> parsedJson){
   return DetailVaccine(
     idDetailVaccine: parsedJson['idDetailVaccine'],
     nameVaccine: parsedJson['nameVaccine'],
     doseVaccine: parsedJson['doseVaccine'],
     dateVaccine: parsedJson['dateVaccine'],
     codeVaccine: parsedJson['codeVaccine'],
     idCardVaccine: parsedJson['idCardVaccine'],
     state: parsedJson['state']
   );
 }

 Map<String, dynamic> toJson() => {
   'idDetailVaccine': idDetailVaccine,
   'nameVaccine': nameVaccine,
   'doseVaccine': doseVaccine,
   'dateVaccine': dateVaccine,
   'codeVaccine': codeVaccine,
   'idCardVaccine':idCardVaccine,
   'state': state
 };

 Map<String, dynamic> toJsonRegistry() => {
   'nameVaccine': nameVaccine,
   'doseVaccine': doseVaccine,
   'dateVaccine': dateVaccine,
   'codeVaccine': codeVaccine,
   'idCardVaccine':idCardVaccine,
   'state': state
};
}