import 'dart:ffi';

class Egress {
  int idEgress;
  DateTime date;
  String type;
  Float value;
  String concept;
  String description;
  int idUser;
  int idPet;

  Egress(
      {this.idEgress,
      this.date,
      this.type,
      this.value,
      this.concept,
      this.description,
      this.idUser,
      this.idPet});

  factory Egress.fromJson(Map<String, dynamic> parsedJson) {
    return Egress(
        idEgress: parsedJson['idEgress'],
        date: parsedJson['date'],
        type: parsedJson['type'],
        value: parsedJson['value'],
        concept: parsedJson['concept'],
        description: parsedJson['description'],
        idUser: parsedJson['idUser'],
        idPet: parsedJson['idPet']);
  }

  Map<String, dynamic> toJson() => {
        'idEgress': idEgress,
        'date': date,
        'type': type,
        'value': value,
        'concept': concept,
        'description': description,
        'idUser': idUser,
        'idPet': idPet
      };

  Map<String, dynamic> toJsonRegistry() => {
        'date': date,
        'type': type,
        'value': value,
        'concept': concept,
        'description': description,
        'idUser': idUser,
        'idPet': idPet
      };
}
