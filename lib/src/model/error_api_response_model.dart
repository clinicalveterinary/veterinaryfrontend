class ErrorApiResponse {
  int statusResponse;
  String message;

  ErrorApiResponse({this.statusResponse, this.message});
  factory ErrorApiResponse.fromJson(Map<String, dynamic> json) {
    return ErrorApiResponse(
        statusResponse: json['statusResponse'], message: json['message']);
  }

  Map<String, dynamic> toJson() =>
      {'statusResponse': statusResponse, 'message': message};
}
