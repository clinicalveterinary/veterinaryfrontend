class ExamDetail{
  int id;
  String name;
  int value;
  int idMedicalExam;


  ExamDetail(
    { this.id,
      this.name,
      this.value,
      this.idMedicalExam,
    }
  );


factory ExamDetail.fromJson(Map<String, dynamic> parsedJson){
  return ExamDetail(
    id: parsedJson['id'],
    name: parsedJson['name'],
    value: parsedJson['value'],
    idMedicalExam: parsedJson['idMedicalExam']
  );
}

Map<String, dynamic> toJson() =>{
'id':id,
'name':name,
'value':value,
'idMedicalExam':idMedicalExam,
};

Map<String, dynamic> toJsonRegistry() =>{
'name':name,
'value':value,
'idMedicalExam':idMedicalExam,
};



}