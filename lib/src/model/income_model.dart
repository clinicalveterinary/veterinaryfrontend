class Income{
  int id;
  DateTime date;
  String type;
  String concept;
  int idIncomeDetail;
  int branchOffice;


  Income(
    { this.id,
      this.date,
      this.type,
      this.idIncomeDetail,
      this.branchOffice
    }
  );


factory Income.fromJson(Map<String, dynamic> parsedJson){
  return Income(
    id: parsedJson['id'],
    date: parsedJson['date'],
    type: parsedJson['type'],
    idIncomeDetail: parsedJson['idIncomeDetail'],
    branchOffice: parsedJson['branchOffice']
  );
}

Map<String, dynamic> toJson() =>{
'id':id,
'date':date,
'type':type,
'idIncomeDetail':idIncomeDetail,
'branchOffice':branchOffice
};

Map<String, dynamic> toJsonRegistry() =>{
'date':date,
'type':type,
'idIncomeDetail':idIncomeDetail,
'branchOffice':branchOffice
};



}