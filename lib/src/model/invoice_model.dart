import 'dart:ffi';

class Invoice {
  int idInvoice;
  DateTime dateInvoice;
  Float value;
  Float discount;
  Float totalPrice;
  int idUser;
  int idBranch;
  int idPet;
  bool status;

  Invoice(
      {this.idInvoice,
      this.dateInvoice,
      this.value,
      this.discount,
      this.totalPrice,
      this.idUser,
      this.idBranch,
      this.idPet,
      this.status});

  factory Invoice.fromJson(Map<String, dynamic> parsedJson) {
    return Invoice(
        idInvoice: parsedJson['idInvoice'],
        dateInvoice: parsedJson['dateInvoice'],
        value: parsedJson['value'],
        discount: parsedJson['discount'],
        totalPrice: parsedJson['totalPrice'],
        idUser: parsedJson['idUser'],
        idBranch: parsedJson['idBranch'],
        idPet: parsedJson['idPet'],
        status: parsedJson['status']);
  }
  Map<String, dynamic> toJson() => {
        'idInvoice': idInvoice,
        'dateInvoice': dateInvoice,
        'value': value,
        'discount': discount,
        'totalPrice': totalPrice,
        'idUser': idUser,
        'idBranch': idBranch,
        'idPet': idPet,
        'status': status
      };

  Map<String, dynamic> toJsonRegistry() => {
        'idInvoice': idInvoice,
        'dateInvoice': dateInvoice,
        'value': value,
        'discount': discount,
        'totalPrice': totalPrice,
        'idUser': idUser,
        'idBranch': idBranch,
        'idPet': idPet,
        'status': status
      };
}
