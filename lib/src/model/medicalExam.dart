class MedicalExam{
  int id;
  int examType;
  int pet; 
  String observation;
  String result;


  MedicalExam(
    { this.id,
      this.examType,
      this.pet,
      this.observation,
      this.result
    }
  );


factory MedicalExam.fromJson(Map<String, dynamic> parsedJson){
  return MedicalExam(
    id: parsedJson['id'],
    examType: parsedJson['examType'],
    pet: parsedJson['pet'],
    observation: parsedJson['observation'],
    result: parsedJson['result']
  );
}

Map<String, dynamic> toJson() =>{
'id':id,
'examType':examType,
'pet':pet,
'observation':observation,
'result':result
};

Map<String, dynamic> toJsonRegistry() =>{
'examType':examType,
'pet':pet,
'observation':observation,
'result':result
};



}