class Pet {
  int idPet;
  String name;
  String species;
  int age;
  String race;
  String sex;
  String observations;
  int idUser;
  bool status;

  Pet(
      {this.idPet,
      this.name,
      this.species,
      this.age,
      this.race,
      this.sex,
      this.observations,
      this.idUser,
      this.status});

  factory Pet.fromJson(Map<String, dynamic> parsedJson) {
    return Pet(
        idPet: parsedJson['idPet'],
        name: parsedJson['name'],
        species: parsedJson['species'],
        age: parsedJson['age'],
        race: parsedJson['race'],
        sex: parsedJson['sex'],
        observations: parsedJson['observations'],
        idUser: parsedJson['idUser'],
        status: parsedJson['status']);
  }

  Map<String, dynamic> toJson() => {
        'idPet': idPet,
        'name': name,
        'species': species,
        'age': age,
        'race': race,
        'sex': sex,
        'observations': observations,
        'idUser': idUser,
        'status': status
      };

  Map<String, dynamic> toJsonRegistry() => {
        'name': name,
        'species': species,
        'age': age,
        'race': race,
        'sex': sex,
        'observations': observations,
        'idUser': idUser,
        'status': status
      };
}
