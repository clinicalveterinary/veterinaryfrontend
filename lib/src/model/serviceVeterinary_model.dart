class ServiceVeterinary {
  int idService;
  String nameService;
  int quantityService;
  double valueService;
  String descriptionService;
  bool statusService;

  ServiceVeterinary(
      {this.idService,
      this.nameService,
      this.quantityService,
      this.valueService,
      this.descriptionService,
      this.statusService});

  factory ServiceVeterinary.fromJson(Map<String, dynamic> parsedJson) {
    return ServiceVeterinary(
        idService: parsedJson['idService'],
        nameService: parsedJson['nameService'],
        quantityService: parsedJson['quantityService'],
        valueService: parsedJson['valueService'],
        descriptionService: parsedJson['descriptionService'],
        statusService: parsedJson['statusService']);
  }
  Map<String, dynamic> toJson() => {
        'idService': idService,
        'nameService': nameService,
        'quantityService': quantityService,
        'valueService': valueService,
        'descriptionService': descriptionService,
        'statusService': statusService
      };

  Map<String, dynamic> toJsonRegistry() => {
        'idService': idService,
        'nameService': nameService,
        'quantityService': quantityService,
        'valueService': valueService,
        'descriptionService': descriptionService,
        'statusService': statusService
      };
}
