class UserVeterinary {
  int idUser;
  String name;
  String lastName;
  String documentType;
  String documentNumber;
  DateTime birthDate;
  String department;
  String city;
  String neighborhood;
  int phone;
  String email;
  String userName;
  String pass;
  bool status;
  int rol;
  String token;

  UserVeterinary(
      {this.idUser,
      this.name,
      this.lastName,
      this.documentType,
      this.documentNumber,
      this.birthDate,
      this.department,
      this.city,
      this.neighborhood,
      this.phone,
      this.email,
      this.userName,
      this.pass,
      this.status,
      this.rol,
      this.token});

  factory UserVeterinary.fromJson(Map<String, dynamic> parsedJson) {
    return UserVeterinary(
        idUser: parsedJson['idUser'],
        name: parsedJson['name'],
        lastName: parsedJson['lastName'],
        documentType: parsedJson['documentType'],
        documentNumber: parsedJson['documentNumber'],
        birthDate: parsedJson['birthDate'],
        department: parsedJson['department'],
        city: parsedJson['city'],
        neighborhood: parsedJson['neighborhood'],
        phone: parsedJson['phone'],
        email: parsedJson['email'],
        userName: parsedJson['userName'],
        pass: parsedJson['pass'],
        status: parsedJson['status'],
        rol: parsedJson['rol'],
        token: parsedJson['token']);
  }

  Map<String, dynamic> toJson() => {
        'idUser': idUser,
        'name': name,
        'lastName': lastName,
        'documentType': documentType,
        'documentNumber': documentNumber,
        'birthDate': birthDate,
        'department': department,
        'city': city,
        'neighborhood': neighborhood,
        'phone': phone,
        'email': email,
        'userName': userName,
        'pass': pass,
        'status': status,
        'rol': rol,
        'token': token
      };

  Map<String, dynamic> toJsonRegistry() => {
        'name': name,
        'lastName': lastName,
        'documentType': documentType,
        'documentNumber': documentNumber,
        'birthDate': birthDate,
        'department': department,
        'city': city,
        'neighborhood': neighborhood,
        'phone': phone,
        'email': email,
        'userName': userName,
        'pass': pass,
        'status': status,
        'rol': rol,
        'token': token
      };
}
