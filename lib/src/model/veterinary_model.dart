class Veterinary {
  int idVeterinary;
  String name;
  int nit;
  String email;
  int owner;
  bool status;

  Veterinary(
      {this.idVeterinary,
      this.name,
      this.nit,
      this.email,
      this.owner,
      this.status});

  factory Veterinary.fromJson(Map<String, dynamic> parsedJson) {
    return Veterinary(
        idVeterinary: parsedJson['idVeterinary'],
        name: parsedJson['name'],
        nit: parsedJson['nit'],
        email: parsedJson['email'],
        owner: parsedJson['owner'],
        status: parsedJson['status']);
  }

  Map<String, dynamic> toJson() => {
        'idVeterinary': idVeterinary,
        'name': name,
        'nit': nit,
        'email': email,
        'owner': owner,
        'status': status
      };

  Map<String, dynamic> toJsonRegistry() => {
        'name': name,
        'nit': nit,
        'email': email,
        'owner': owner,
        'status': status
      };
}
