import 'dart:convert';
import 'dart:io';

import 'package:clinical_veterinary/src/model/api_response_model.dart';
import 'package:clinical_veterinary/src/model/appointment_model.dart';
import 'package:clinical_veterinary/src/model/error_api_response_model.dart';
import 'package:clinical_veterinary/src/utils/Constants.dart';
import 'package:http/http.dart' as http;

class AppointmentApiService {
  Appointment _appointment;
  ErrorApiResponse _error;
  AppointmentApiService();

  Future<ApiResponse> getAllByTopicAppointment(
      String topicVaccine, String accessToken) async {
    var apiResponse = ApiResponse(statusResponse: 0);
    var queryParameters = {
      'topicVaccine': topicVaccine,
    };

    var url = Uri.http(Constants.urlAuthority,
        Constants.urlgetAllByTopicAppointment, queryParameters);
    var resp = await http.get(url,
        headers: {HttpHeaders.authorizationHeader: 'Bearer' + accessToken});
    var respBody = json.decode(resp.body);
    apiResponse.statusResponse = resp.statusCode;

    if (apiResponse.statusResponse == 200) {
      _appointment = Appointment.fromJson(respBody);
      apiResponse.payload = _appointment;
    } else {
      _error = ErrorApiResponse.fromJson(respBody);
      apiResponse.payload = _error;
    }
    return apiResponse;
  }
}
