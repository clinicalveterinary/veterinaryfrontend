import 'dart:convert';
import 'dart:io';

import 'package:clinical_veterinary/src/model/api_response_model.dart';
import 'package:clinical_veterinary/src/model/branchOffice_model.dart';
import 'package:clinical_veterinary/src/model/error_api_response_model.dart';
import 'package:clinical_veterinary/src/utils/Constants.dart';
import 'package:http/http.dart' as http;

class BranchOfficeApiService {
  BranchOffice _branchOffice;
  ErrorApiResponse _error;
  BranchOfficeApiService();

  Future<ApiResponse> createBranchOffice(
      BranchOffice branchOffice, String accessToken) async {
    var apiResponse = ApiResponse(statusResponse: 0);
    var body = json.encode(branchOffice.toJsonRegistry());
    var uri = Uri.http(Constants.urlAuthority, Constants.urlcreateBranchOffice);
    var res = await http.post(uri,
        headers: {
          HttpHeaders.contentTypeHeader: 'application/json',
          HttpHeaders.authorizationHeader: 'Bearer ' + accessToken
        },
        body: body);

    var resBody = json.decode(res.body);
    apiResponse.statusResponse = res.statusCode;
    if (apiResponse.statusResponse == 200) {
      _branchOffice = BranchOffice.fromJson(resBody);
      apiResponse.payload = _branchOffice;
    } else {
      _error = ErrorApiResponse.fromJson(resBody);
      apiResponse.payload = _error;
    }
    return apiResponse;
  }

  Future<ApiResponse> updateBranchOffice(
      BranchOffice branchOffice, String accessToken) async {
    var apiResponse = ApiResponse(statusResponse: 0);
    var body = json.encode(branchOffice.toJson());
    var uri = Uri.http(Constants.urlAuthority, Constants.urlupdateBranchOffice);
    var res = await http.put(uri,
        headers: {
          HttpHeaders.contentTypeHeader: 'application/json',
          HttpHeaders.authorizationHeader: 'Bearer ' + accessToken
        },
        body: body);

    var resBody = json.decode(res.body);
    apiResponse.statusResponse = res.statusCode;
    if (apiResponse.statusResponse == 200) {
      _branchOffice = BranchOffice.fromJson(resBody);
      apiResponse.payload = _branchOffice;
    } else {
      _error = ErrorApiResponse.fromJson(resBody);
      apiResponse.payload = _error;
    }
    return apiResponse;
  }

  Future<ApiResponse> getAllBranchOffice(String accessToken) async {
    List<BranchOffice> listBranch;
    listBranch = [];
    var apiResponse = ApiResponse(statusResponse: 0);
    var url = Uri.http(Constants.urlAuthority, Constants.urlgetAllBranchOffice);
    var res = await http.get(url,
        headers: {HttpHeaders.authorizationHeader: 'Bearer ' + accessToken});
    var resBody = json.decode(res.body);

    apiResponse.statusResponse = res.statusCode;

    listBranch = [];
    if (apiResponse.statusResponse == 200) {
      resBody.forEach((branch) {
        listBranch.add(branch.fromJson(branch));
        return branch;
      });
      apiResponse.payload = listBranch;
    } else {
      _error = ErrorApiResponse.fromJson(resBody);
      apiResponse.payload = _error;
    }

    return apiResponse;
  }

  Future<ApiResponse> deleteBranchOffice(int id, String accessToken) async {
    var apiResponse = ApiResponse(statusResponse: 0);
    var queryParameters = {
      'id': id.toString(),
    };
    var uri = Uri.http(Constants.urlAuthority, Constants.urldeleteBranchOffice,
        queryParameters);
    var res = await http.delete(uri,
        headers: {HttpHeaders.authorizationHeader: 'Bearer ' + accessToken});

    var resBody = json.decode(res.body);
    apiResponse.statusResponse = res.statusCode;
    if (apiResponse.statusResponse == 200) {
      _branchOffice = BranchOffice.fromJson(resBody);
      apiResponse.payload = _branchOffice;
    } else {
      _error = ErrorApiResponse.fromJson(resBody);
      apiResponse.payload = _error;
    }

    return apiResponse;
  }
}
