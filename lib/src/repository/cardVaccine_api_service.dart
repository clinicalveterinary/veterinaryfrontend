import 'dart:convert';
import 'dart:io';

import 'package:clinical_veterinary/src/model/api_response_model.dart';
import 'package:clinical_veterinary/src/model/cardVaccine_model.dart';
import 'package:clinical_veterinary/src/model/error_api_response_model.dart';
import 'package:clinical_veterinary/src/utils/Constants.dart';
import 'package:http/http.dart' as http;

class CardVaccineApiService {
  CardVaccine _cardVaccine;
  ErrorApiResponse _error;
  CardVaccineApiService();

  Future<ApiResponse> getOneByIdPet(int idpet, String accessToken) async {
    var apiResponse = ApiResponse(statusResponse: 0);
    var queryParameters = {
      'idpet': idpet.toString(),
    };

    var url = Uri.http(
        Constants.urlAuthority, Constants.urlgetOneByIdPet, queryParameters);
    var resp = await http.get(url,
        headers: {HttpHeaders.authorizationHeader: 'Bearer' + accessToken});
    var respBody = json.decode(resp.body);
    apiResponse.statusResponse = resp.statusCode;

    if (apiResponse.statusResponse == 200) {
      _cardVaccine = CardVaccine.fromJson(respBody);
      apiResponse.payload = _cardVaccine;
    } else {
      _error = ErrorApiResponse.fromJson(respBody);
      apiResponse.payload = _error;
    }
    return apiResponse;
  }

  Future<ApiResponse> getCardVaccine(int idpet, String accessToken) async {
    var apiResponse = ApiResponse(statusResponse: 0);
    var queryParameters = {
      'idpet': idpet.toString(),
    };

    var url = Uri.http(
        Constants.urlAuthority, Constants.urlgetCardVaccine, queryParameters);
    var resp = await http.get(url,
        headers: {HttpHeaders.authorizationHeader: 'Bearer' + accessToken});
    var respBody = json.decode(resp.body);
    apiResponse.statusResponse = resp.statusCode;

    if (apiResponse.statusResponse == 200) {
      _cardVaccine = CardVaccine.fromJson(respBody);
      apiResponse.payload = _cardVaccine;
    } else {
      _error = ErrorApiResponse.fromJson(respBody);
      apiResponse.payload = _error;
    }
    return apiResponse;
  }

  Future<ApiResponse> sendMailPDF(
      String email, int idPet, String accessToken) async {
    var apiResponse = ApiResponse(statusResponse: 0);
    var queryParameters = {
      'email': email,
      'idPet': idPet.toString(),
    };

    var url = Uri.http(
        Constants.urlAuthority, Constants.urlsendMailPDF, queryParameters);
    var resp = await http.get(url,
        headers: {HttpHeaders.authorizationHeader: 'Bearer' + accessToken});
    var respBody = json.decode(resp.body);
    apiResponse.statusResponse = resp.statusCode;

    if (apiResponse.statusResponse == 200) {
      _cardVaccine = CardVaccine.fromJson(respBody);
      apiResponse.payload = _cardVaccine;
    } else {
      _error = ErrorApiResponse.fromJson(respBody);
      apiResponse.payload = _error;
    }
    return apiResponse;
  }
}
