import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' as http;
import 'package:clinical_veterinary/src/model/api_response_model.dart';
import 'package:clinical_veterinary/src/model/category_model.dart';
import 'package:clinical_veterinary/src/model/error_api_response_model.dart';
import 'package:clinical_veterinary/src/utils/Constants.dart';

class CategoryAPIService {
  Category _category;
  ErrorApiResponse _error;
  CategoryAPIService();

  Future<ApiResponse> getCategoryByName(String name, String accessToken) async {
    var apiResponse = ApiResponse(statusResponse: 0);
    var queryParameters = {
      'name': name.toString(),
    };

    var url = Uri.http(Constants.urlAuthority, Constants.urlCategoryGetByName,
        queryParameters);
    var res = await http.get(url, headers: {
      HttpHeaders.authorizationHeader:
          Constants.authorizationHeader + accessToken
    });

    var resBody;
    apiResponse.statusResponse = res.statusCode;
    if (res.body.isNotEmpty) {
      resBody = json.decode(res.body);
      if (apiResponse.statusResponse == 200) {
        _category = Category.fromJson(resBody);
        apiResponse.payload = _category;
      } else {
        _error = ErrorApiResponse.fromJson(resBody);
        apiResponse.payload = _error;
      }
    }
    return apiResponse;
  }

  Future<ApiResponse> insertCateogry(
      Category category, String accessToken) async {
    var apiResponse = ApiResponse(statusResponse: 0);
    var body = json.encode(category.toJsonRegistry());

    var uri = Uri.http(Constants.urlAuthority, Constants.urlCategorySave);
    var res = await http.post(uri,
        headers: {
          HttpHeaders.contentTypeHeader: Constants.contentTypeHeader,
          HttpHeaders.authorizationHeader:
              Constants.authorizationHeader + accessToken
        },
        body: body);
    var resBody = json.decode(res.body);
    apiResponse.statusResponse = res.statusCode;
    if (apiResponse.statusResponse == 200) {
      _category = Category.fromJson(resBody);
      apiResponse.payload = _category;
    } else {
      _error = ErrorApiResponse.fromJson(resBody);
      apiResponse.payload = _error;
    }

    return apiResponse;
  }

  Future<ApiResponse> updateCategory(
      Category category, String accessToken) async {
    var apiResponse = ApiResponse(statusResponse: 0);
    var body = json.encode(category.toJson());
    var uri = Uri.http(Constants.urlAuthority, Constants.urlCategoryUpdate);
    var res = await http.put(uri,
        headers: {
          HttpHeaders.contentTypeHeader: Constants.contentTypeHeader,
          HttpHeaders.authorizationHeader:
              Constants.authorizationHeader + accessToken
        },
        body: body);
    var resBody = json.decode(res.body);
    apiResponse.statusResponse = res.statusCode;
    if (apiResponse.statusResponse == 200) {
      _category = Category.fromJson(resBody);
      apiResponse.payload = _category;
    } else {
      _error = ErrorApiResponse.fromJson(resBody);
      apiResponse.payload = _error;
    }

    return apiResponse;
  }
}
