import 'dart:convert';
import 'dart:io';

import 'package:clinical_veterinary/src/model/api_response_model.dart';
import 'package:clinical_veterinary/src/model/clinicalHistory_model.dart';
import 'package:clinical_veterinary/src/model/error_api_response_model.dart';
import 'package:clinical_veterinary/src/utils/Constants.dart';
import 'package:http/http.dart' as http;

class ClinicalHistoryApiServices {
  ClinicalHistory _clinicalHistory;
  ErrorApiResponse _error;
  ClinicalHistoryApiServices();

  Future<ApiResponse> createClinicalHistory(
      ClinicalHistory clinicalHistory, String accessToken) async {
    ApiResponse apiResponse;
    apiResponse = ApiResponse(statusResponse: 0);
    var body = json.encode(clinicalHistory.toJsonRegistry());
    Uri uri;
    uri = Uri.http(Constants.urlAuthority, Constants.urlcreateClinicalHistory);
    var res = await http.post(uri,
        headers: {
          HttpHeaders.contentTypeHeader: 'application/json',
          HttpHeaders.authorizationHeader: 'Bearer ' + accessToken
        },
        body: body);

    var resBody = json.decode(res.body);
    apiResponse.statusResponse = res.statusCode;
    if (apiResponse.statusResponse == 200) {
      _clinicalHistory = ClinicalHistory.fromJson(resBody);
      apiResponse.payload = _clinicalHistory;
    } else {
      _error = ErrorApiResponse.fromJson(resBody);
      apiResponse.payload = _error;
    }
    return apiResponse;
  }
}
