import 'dart:convert';
import 'dart:io';
import 'package:clinical_veterinary/src/model/api_response_model.dart';
import 'package:clinical_veterinary/src/model/detailInvoice_model.dart';
import 'package:clinical_veterinary/src/model/error_api_response_model.dart';
import 'package:clinical_veterinary/src/utils/Constants.dart';
import 'package:http/http.dart' as http;

class DetailInvoiceApiService {
  DetailInvoice _detailInvoice;
  ErrorApiResponse _error;
  DetailInvoiceApiService();

  Future<ApiResponse> getAllByidInvoice(int id, String accessToken) async {
    ApiResponse apiResponse;
    apiResponse = ApiResponse(statusResponse: 0);
    var queryParameters = {
      'id': id.toString(),
    };
    Uri url;
    url = Uri.http(Constants.urlAuthority, Constants.urlfindAllByIdInvoice,
        queryParameters);
    var resp = await http.get(url,
        headers: {HttpHeaders.authorizationHeader: 'Bearer' + accessToken});
    var respBody = json.decode(resp.body);
    apiResponse.statusResponse = resp.statusCode;

    if (apiResponse.statusResponse == 200) {
      _detailInvoice = DetailInvoice.fromJson(respBody);
      apiResponse.payload = _detailInvoice;
    } else {
      _error = ErrorApiResponse.fromJson(respBody);
      apiResponse.payload = _error;
    }
    return apiResponse;
  }
}
