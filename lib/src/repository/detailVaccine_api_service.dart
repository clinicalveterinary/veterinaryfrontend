import 'dart:convert';
import 'dart:io';

import 'package:clinical_veterinary/src/model/api_response_model.dart';
import 'package:clinical_veterinary/src/model/detailVaccine_model.dart';
import 'package:clinical_veterinary/src/model/error_api_response_model.dart';
import 'package:clinical_veterinary/src/utils/Constants.dart';
import 'package:http/http.dart' as http;

class DetailVaccineApiService {
  DetailVaccine _detailVaccine;
  ErrorApiResponse _error;
  DetailVaccineApiService();

  Future<ApiResponse> getAllByIdCardVaccine(
      int idcardvaccine, String accessToken) async {
    ApiResponse apiResponse;
    apiResponse = ApiResponse(statusResponse: 0);
    var queryParameters = {
      'idcardvaccine': idcardvaccine.toString(),
    };

    Uri url;
    url = Uri.http(Constants.urlAuthority, Constants.urlgetAllByIdCardVaccine,
        queryParameters);
    var resp = await http.get(url,
        headers: {HttpHeaders.authorizationHeader: 'Bearer' + accessToken});
    var respBody = json.decode(resp.body);
    apiResponse.statusResponse = resp.statusCode;

    if (apiResponse.statusResponse == 200) {
      _detailVaccine = DetailVaccine.fromJson(respBody);
      apiResponse.payload = _detailVaccine;
    } else {
      _error = ErrorApiResponse.fromJson(respBody);
      apiResponse.payload = _error;
    }
    return apiResponse;
  }
}
