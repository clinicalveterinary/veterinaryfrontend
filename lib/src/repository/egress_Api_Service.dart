import 'dart:convert';
import 'dart:io';

import 'package:clinical_veterinary/src/model/api_response_model.dart';
import 'package:clinical_veterinary/src/model/egress_model.dart';
import 'package:clinical_veterinary/src/model/error_api_response_model.dart';
import 'package:clinical_veterinary/src/utils/Constants.dart';
import 'package:http/http.dart' as http;

class EgressApiService {
  Egress _egress;
  ErrorApiResponse _error;
  EgressApiService();

  Future<ApiResponse> getAllEgress(String accessToken) async {
    List<Egress> listEgress;
    listEgress = [];
    ApiResponse apiResponse;
    apiResponse = ApiResponse(statusResponse: 0);
    Uri url;
    url = Uri.http(Constants.urlAuthority, Constants.urlgetAllEgress);
    var res = await http.get(url,
        headers: {HttpHeaders.authorizationHeader: 'Bearer ' + accessToken});
    var resBody = json.decode(res.body);

    apiResponse.statusResponse = res.statusCode;

    listEgress = [];
    if (apiResponse.statusResponse == 200) {
      resBody.forEach((egress) {
        listEgress.add(egress.fromJson(egress));
        return egress;
      });
      apiResponse.payload = listEgress;
    } else {
      _error = ErrorApiResponse.fromJson(resBody);
      apiResponse.payload = _error;
    }

    return apiResponse;
  }

  Future<ApiResponse> getAllEgressByType(
      String type, String accessToken) async {
    ApiResponse apiResponse;
    apiResponse = ApiResponse(statusResponse: 0);
    var queryParameters = {
      'type': type,
    };
    Uri uri;
    uri = Uri.http(Constants.urlAuthority, Constants.urlgetAllEgressByType,
        queryParameters);
    var res = await http.get(uri,
        headers: {HttpHeaders.authorizationHeader: 'Bearer ' + accessToken});

    var resBody = json.decode(res.body);
    apiResponse.statusResponse = res.statusCode;
    if (apiResponse.statusResponse == 200) {
      _egress = Egress.fromJson(resBody);
      apiResponse.payload = _egress;
    } else {
      _error = ErrorApiResponse.fromJson(resBody);
      apiResponse.payload = _error;
    }

    return apiResponse;
  }

  Future<ApiResponse> getAllEgressByPet(int idPet, String accessToken) async {
    ApiResponse apiResponse;
    apiResponse = ApiResponse(statusResponse: 0);
    var queryParameters = {
      'idPet': idPet.toString(),
    };
    Uri uri;
    uri = Uri.http(Constants.urlAuthority, Constants.urlgetAllEgressByPet,
        queryParameters);
    var res = await http.get(uri,
        headers: {HttpHeaders.authorizationHeader: 'Bearer ' + accessToken});

    var resBody = json.decode(res.body);
    apiResponse.statusResponse = res.statusCode;
    if (apiResponse.statusResponse == 200) {
      _egress = Egress.fromJson(resBody);
      apiResponse.payload = _egress;
    } else {
      _error = ErrorApiResponse.fromJson(resBody);
      apiResponse.payload = _error;
    }

    return apiResponse;
  }

  Future<ApiResponse> createEgress(Egress egress, String accessToken) async {
    ApiResponse apiResponse;
    apiResponse = ApiResponse(statusResponse: 0);
    var body = json.encode(egress.toJsonRegistry());
    Uri uri;
    uri = Uri.http(Constants.urlAuthority, Constants.urlcreateEgress);
    var res = await http.post(uri,
        headers: {
          HttpHeaders.contentTypeHeader: 'application/json',
          HttpHeaders.authorizationHeader: 'Bearer ' + accessToken
        },
        body: body);

    var resBody = json.decode(res.body);
    apiResponse.statusResponse = res.statusCode;
    if (apiResponse.statusResponse == 200) {
      _egress = Egress.fromJson(resBody);
      apiResponse.payload = _egress;
    } else {
      _error = ErrorApiResponse.fromJson(resBody);
      apiResponse.payload = _error;
    }
    return apiResponse;
  }

  Future<ApiResponse> updateEgress(Egress egress, String accessToken) async {
    ApiResponse apiResponse;
    apiResponse = ApiResponse(statusResponse: 0);
    var body = json.encode(egress.toJson());
    Uri uri;
    uri = Uri.http(Constants.urlAuthority, Constants.urlupdateEgress);
    var res = await http.put(uri,
        headers: {
          HttpHeaders.contentTypeHeader: 'application/json',
          HttpHeaders.authorizationHeader: 'Bearer ' + accessToken
        },
        body: body);

    var resBody = json.decode(res.body);
    apiResponse.statusResponse = res.statusCode;
    if (apiResponse.statusResponse == 200) {
      _egress = Egress.fromJson(resBody);
      apiResponse.payload = _egress;
    } else {
      _error = ErrorApiResponse.fromJson(resBody);
      apiResponse.payload = _error;
    }
    return apiResponse;
  }
}
