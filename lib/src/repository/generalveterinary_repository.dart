import 'package:clinical_veterinary/src/model/api_response_model.dart';
import 'package:clinical_veterinary/src/model/branchOffice_model.dart';
import 'package:clinical_veterinary/src/model/category_model.dart';
import 'package:clinical_veterinary/src/model/clinicalHistoryDetail_model.dart';
import 'package:clinical_veterinary/src/model/clinicalHistory_model.dart';
import 'package:clinical_veterinary/src/model/egress_model.dart';
import 'package:clinical_veterinary/src/model/veterinary_model.dart';
import 'package:clinical_veterinary/src/repository/appointment_api_service.dart';
import 'package:clinical_veterinary/src/repository/branchoffice_api_service.dart';
import 'package:clinical_veterinary/src/repository/category_API_Service.dart';
import 'package:clinical_veterinary/src/repository/clinicalhistory_api_service.dart';
import 'package:clinical_veterinary/src/repository/clinicalhistorydetail_api_service.dart';
import 'package:clinical_veterinary/src/repository/income_API_Service.dart';
import 'package:clinical_veterinary/src/repository/medicalExam_API_Service.dart';
import 'package:clinical_veterinary/src/repository/cardVaccine_api_service.dart';
import 'package:clinical_veterinary/src/repository/detailInvoice_API_Service.dart';
import 'package:clinical_veterinary/src/repository/detailVaccine_api_service.dart';
import 'package:clinical_veterinary/src/repository/egress_api_service.dart';
import 'package:clinical_veterinary/src/repository/invoice_API_Service.dart';
import 'package:clinical_veterinary/src/repository/pet_api_service.dart';
import 'package:clinical_veterinary/src/repository/serviceVeterinary_API_service.dart';
import 'package:clinical_veterinary/src/repository/veterinary_api_service.dart';

class GeneralVeterinaryRepository {
  final egressApiService = EgressApiService();
  final clinicalHistoryApiServices = ClinicalHistoryApiServices();
  final clinicalHistoryDetailApiService = ClinicalHistoryDetailApiService();
  final petApiService = PetApiService();
  final branchOfficeApiService = BranchOfficeApiService();
  final veterinaryApiService = VeterinaryApiService();
  final incomeAPIService = IncomeAPIService();
  final categoryApiService = CategoryAPIService();
  final medicalExam = MedicalExamAPIService();
  final cardVaccineApiService = CardVaccineApiService();
  final appointmentApiService = AppointmentApiService();
  final detailVaccineApiService = DetailVaccineApiService();
  final invoiceApiService = InvoiceApiService();
  final detailInvoiceApiService = DetailInvoiceApiService();
  final serviceVeterinaryApiService = ServiceVeterinaryApiService();

  String accessToken;

//egress
  Future<ApiResponse> getAllEgress() =>
      egressApiService.getAllEgress(accessToken);
  Future<ApiResponse> getAllEgressByType(String type) =>
      egressApiService.getAllEgressByType(type, accessToken);
  Future<ApiResponse> getAllEgressByPet(int idPet) =>
      egressApiService.getAllEgressByPet(idPet, accessToken);
  Future<ApiResponse> createEgress(Egress egress) =>
      egressApiService.createEgress(egress, accessToken);
  Future<ApiResponse> updateEgress(Egress egress) =>
      egressApiService.updateEgress(egress, accessToken);

//Income
  Future<ApiResponse> getAllIncome() =>
      incomeAPIService.getAllIncome(accessToken);
//Category
  Future<ApiResponse> getCategoryByName(String name) =>
      categoryApiService.getCategoryByName(name, accessToken);
  Future<ApiResponse> insertCateogry(Category category) =>
      categoryApiService.insertCateogry(category, accessToken);
  Future<ApiResponse> updateCategory(Category category) =>
      categoryApiService.updateCategory(category, accessToken);
//MedicalExam
  Future<ApiResponse> sendEmailWithPDF(String email, int id) =>
      medicalExam.sendEmailWithPDF(accessToken, email, id);

//CH

  Future<ApiResponse> createClinicalHistory(ClinicalHistory clinicalHistory) =>
      clinicalHistoryApiServices.createClinicalHistory(
          clinicalHistory, accessToken);

//CHD

  Future<ApiResponse> createClinicalhistoryDetail(
          ClinicalHistoryDetail clinicalHistoryDetail) =>
      clinicalHistoryDetailApiService.createClinicalhistoryDetail(
          clinicalHistoryDetail, accessToken);

//pet

  Future<ApiResponse> getAllPetByUser(String document) =>
      petApiService.getAllPetByUser(document, accessToken);

//BO

  Future<ApiResponse> createBranchOffice(BranchOffice branchOffice) =>
      branchOfficeApiService.createBranchOffice(branchOffice, accessToken);
  Future<ApiResponse> updateBranchOffice(BranchOffice branchOffice) =>
      branchOfficeApiService.updateBranchOffice(branchOffice, accessToken);
  Future<ApiResponse> getAllBranchOffice() =>
      branchOfficeApiService.getAllBranchOffice(accessToken);
  Future<ApiResponse> deleteBranchOffice(int id) =>
      branchOfficeApiService.deleteBranchOffice(id, accessToken);

//veterinary

  Future<ApiResponse> createVeterinary(Veterinary veterinary) =>
      veterinaryApiService.createVeterinary(veterinary, accessToken);
  Future<ApiResponse> updateVeterinary(Veterinary veterinary) =>
      veterinaryApiService.updateVeterinary(veterinary, accessToken);
  Future<ApiResponse> getAllVeterinary() =>
      veterinaryApiService.getAllVeterinary(accessToken);
  Future<ApiResponse> getAllVeterinaryByName(String name) =>
      veterinaryApiService.getAllVeterinaryByName(name, accessToken);
  Future<ApiResponse> deleteVeterinary(int id) =>
      veterinaryApiService.deleteVeterinary(id, accessToken);

//cardVaccine
  Future<ApiResponse> getOneByIdPet(int idpet, accessToken) =>
      cardVaccineApiService.getOneByIdPet(idpet, accessToken);
  Future<ApiResponse> getCardVaccine(int idpet, accessToken) =>
      cardVaccineApiService.getCardVaccine(idpet, accessToken);
  Future<ApiResponse> sendMailPDF(String email, int idPet, accessToken) =>
      cardVaccineApiService.sendMailPDF(email, idPet, accessToken);

//DetailVaccine
  Future<ApiResponse> getAllByIdCardVaccine(int idcardvaccine, accessToken) =>
      detailVaccineApiService.getAllByIdCardVaccine(idcardvaccine, accessToken);

//Appointment
  Future<ApiResponse> getAllByTopicAppointment(String topicVaccine) =>
      appointmentApiService.getAllByTopicAppointment(topicVaccine, accessToken);
//invoice
  Future<ApiResponse> findAllByIdUser(int idUser) =>
      invoiceApiService.getAllByIdUser(idUser, accessToken);
  Future<ApiResponse> findAllByIdPet(int idPet) =>
      invoiceApiService.getAllByIdPet(idPet, accessToken);
  Future<ApiResponse> getOneById(int idInvoice) =>
      invoiceApiService.getOneByIdInvoice(idInvoice, accessToken);
  Future<ApiResponse> updateInvoice(invoice, accessToken) =>
      invoiceApiService.updateInvoice(invoice, accessToken);
  Future<ApiResponse> sendEmailByIdIvnoice(
          String email, int idInvoice, String accessToken) =>
      invoiceApiService.sendEmailByIdIvnoice(email, idInvoice, accessToken);
//DetailInvoice
  Future<ApiResponse> getAllByidInvoice(int id) =>
      detailInvoiceApiService.getAllByidInvoice(id, accessToken);
//ServiceVeterinary
  Future<ApiResponse> allVeterinaryByName(String name, accessToken) =>
      serviceVeterinaryApiService.getAllVeterinaryByName(name, accessToken);
}
