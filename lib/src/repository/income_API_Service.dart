import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' as http;
import 'package:clinical_veterinary/src/model/api_response_model.dart';
import 'package:clinical_veterinary/src/model/error_api_response_model.dart';
import 'package:clinical_veterinary/src/model/income_model.dart';
import 'package:clinical_veterinary/src/utils/Constants.dart';

class IncomeAPIService {
  ErrorApiResponse _error;
  IncomeAPIService();

  Future<ApiResponse> getAllIncome(String accessToken) async {
    List<Income> listIncome;
    ApiResponse apiResponse;
    apiResponse = ApiResponse(statusResponse: 0);
    Uri uri;
    uri = Uri.http(Constants.urlAuthority, Constants.urlIncomeGetAll);
    var res = await http.get(uri, headers: {
      HttpHeaders.authorizationHeader:
          Constants.authorizationHeader + accessToken
    });
    var resBody = json.decode(res.body);
    apiResponse.statusResponse = res.statusCode;

    listIncome = [];
    if (apiResponse.statusResponse == 200) {
      resBody.forEach((i) {
        listIncome.add(Income.fromJson(i));
        return i;
      });
      apiResponse.payload = listIncome;
    } else {
      _error = ErrorApiResponse.fromJson(resBody);
      apiResponse.payload = _error;
    }
    return apiResponse;
  }
}
