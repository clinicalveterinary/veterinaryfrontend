import 'dart:convert';
import 'dart:io';
import 'package:clinical_veterinary/src/model/Invoice_model.dart';
import 'package:clinical_veterinary/src/model/api_response_model.dart';
import 'package:clinical_veterinary/src/model/error_api_response_model.dart';
import 'package:clinical_veterinary/src/utils/Constants.dart';
import 'package:http/http.dart' as http;

class InvoiceApiService {
  Invoice _invoice;
  ErrorApiResponse _error;
  InvoiceApiService();

  Future<ApiResponse> getAllByIdUser(int idUser, String accessToken) async {
    var apiResponse = ApiResponse(statusResponse: 0);
    var queryParameters = {
      'idUser': idUser.toString(),
    };
    var url = Uri.http(Constants.urlAuthority, Constants.urlfindAllByIdInvoice,
        queryParameters);
    var resp = await http.get(url,
        headers: {HttpHeaders.authorizationHeader: 'Bearer ' + accessToken});
    var respBody = json.decode(resp.body);
    apiResponse.statusResponse = resp.statusCode;

    if (apiResponse.statusResponse == 200) {
      _invoice = Invoice.fromJson(respBody);
      apiResponse.payload = _invoice;
    } else {
      _error = ErrorApiResponse.fromJson(respBody);
      apiResponse.payload = _error;
    }
    return apiResponse;
  }

  Future<ApiResponse> getAllByIdPet(int idPet, String accessToken) async {
    var apiResponse = ApiResponse(statusResponse: 0);
    var queryParameters = {
      'idPet': idPet.toString(),
    };
    var url = Uri.http(Constants.urlAuthority, Constants.urlfindAllByIdInvoice,
        queryParameters);
    var resp = await http.get(url,
        headers: {HttpHeaders.authorizationHeader: 'Bearer ' + accessToken});
    var respBody = json.decode(resp.body);
    apiResponse.statusResponse = resp.statusCode;

    if (apiResponse.statusResponse == 200) {
      _invoice = Invoice.fromJson(respBody);
      apiResponse.payload = _invoice;
    } else {
      _error = ErrorApiResponse.fromJson(respBody);
      apiResponse.payload = _error;
    }
    return apiResponse;
  }

  Future<ApiResponse> getOneByIdInvoice(
      int idInvoice, String accessToken) async {
    var apiResponse = ApiResponse(statusResponse: 0);
    var queryParameters = {
      'idInvoice': idInvoice.toString(),
    };

    var url = Uri.http(
        Constants.urlAuthority, Constants.urlgetOneById, queryParameters);
    var resp = await http.get(url,
        headers: {HttpHeaders.authorizationHeader: 'Bearer' + accessToken});
    var respBody = json.decode(resp.body);
    apiResponse.statusResponse = resp.statusCode;

    if (apiResponse.statusResponse == 200) {
      _invoice = Invoice.fromJson(respBody);
      apiResponse.payload = _invoice;
    } else {
      _error = ErrorApiResponse.fromJson(respBody);
      apiResponse.payload = _error;
    }
    return apiResponse;
  }

  Future<ApiResponse> updateInvoice(Invoice invoice, String accessToken) async {
    var apiResponse = ApiResponse(statusResponse: 0);
    var body = json.encode(invoice.toJson());
    var url = Uri.http(Constants.urlAuthority, Constants.urlinvoiceUpate);
    var resp = await http.put(url,
        headers: {
          HttpHeaders.contentTypeHeader: 'application/json',
          HttpHeaders.authorizationHeader: 'Bearer' + accessToken
        },
        body: body);
    var respBody = json.decode(resp.body);
    apiResponse.statusResponse = resp.statusCode;

    if (apiResponse.statusResponse == 200) {
      _invoice = Invoice.fromJson(respBody);
      apiResponse.payload = _invoice;
    } else {
      _error = ErrorApiResponse.fromJson(respBody);
      apiResponse.payload = _error;
    }
    return apiResponse;
  }

  Future<ApiResponse> sendEmailByIdIvnoice(
      String email, int idInvoice, String accessToken) async {
    var apiResponse = ApiResponse(statusResponse: 0);
    var queryParameters = {
      'email': email,
      'idInvoice': idInvoice.toString(),
    };

    var url = Uri.http(Constants.urlAuthority,
        Constants.urlsendEmailByIdIvnoice, queryParameters);
    var resp = await http.get(url,
        headers: {HttpHeaders.authorizationHeader: 'Bearer' + accessToken});
    var respBody = json.decode(resp.body);
    apiResponse.statusResponse = resp.statusCode;

    if (apiResponse.statusResponse == 200) {
      _invoice = Invoice.fromJson(respBody);
      apiResponse.payload = _invoice;
    } else {
      _error = ErrorApiResponse.fromJson(respBody);
      apiResponse.payload = _error;
    }
    return apiResponse;
  }
}
