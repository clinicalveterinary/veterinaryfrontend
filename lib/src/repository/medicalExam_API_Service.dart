import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' as http;
import 'package:clinical_veterinary/src/model/api_response_model.dart';
import 'package:clinical_veterinary/src/model/error_api_response_model.dart';
import 'package:clinical_veterinary/src/utils/Constants.dart';

class MedicalExamAPIService {
  ErrorApiResponse _error;
  MedicalExamAPIService();

  Future<ApiResponse> sendEmailWithPDF(
      String accessToken, String email, int id) async {
    var apiResponse = ApiResponse(statusResponse: 0);
    var queryParameters = {
      'email': email.toString(),
    };

    var url = Uri.http(Constants.urlAuthority, Constants.urlMedicalExamPDFEmail,
        queryParameters);
    var res = await http.get(url, headers: {
      HttpHeaders.authorizationHeader:
          Constants.authorizationHeader + accessToken
    });

    var resBody;
    apiResponse.statusResponse = res.statusCode;
    if (res.body.isNotEmpty) {
      resBody = json.decode(res.body);
      if (apiResponse.statusResponse == 200) {
        apiResponse.payload = resBody;
      } else {
        _error = ErrorApiResponse.fromJson(resBody);
        apiResponse.payload = _error;
      }
    }
    return apiResponse;
  }
}
