import 'dart:convert';
import 'dart:io';

import 'package:clinical_veterinary/src/model/api_response_model.dart';
import 'package:clinical_veterinary/src/model/error_api_response_model.dart';
import 'package:clinical_veterinary/src/model/pet_model.dart';
import 'package:clinical_veterinary/src/utils/Constants.dart';
import 'package:http/http.dart' as http;

class PetApiService {
  Pet _pet;
  ErrorApiResponse _error;
  PetApiService();

  Future<ApiResponse> getAllPetByUser(
      String document, String accessToken) async {
    var apiResponse = ApiResponse(statusResponse: 0);
    var queryParameters = {
      'identificacion': document,
    };
    var uri = Uri.http(
        Constants.urlAuthority, Constants.urlgetAllPetByUser, queryParameters);
    var res = await http.get(uri,
        headers: {HttpHeaders.authorizationHeader: 'Bearer ' + accessToken});

    var resBody = json.decode(res.body);
    apiResponse.statusResponse = res.statusCode;
    if (apiResponse.statusResponse == 200) {
      _pet = Pet.fromJson(resBody);
      apiResponse.payload = _pet;
    } else {
      _error = ErrorApiResponse.fromJson(resBody);
      apiResponse.payload = _error;
    }
    return apiResponse;
  }
}
