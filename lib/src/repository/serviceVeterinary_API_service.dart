import 'dart:convert';
import 'dart:io';
import 'package:clinical_veterinary/src/model/api_response_model.dart';
import 'package:clinical_veterinary/src/model/error_api_response_model.dart';
import 'package:clinical_veterinary/src/model/serviceVeterinary_model.dart';
import 'package:clinical_veterinary/src/utils/Constants.dart';
import 'package:http/http.dart' as http;

class ServiceVeterinaryApiService {
  ServiceVeterinary _serviceVeterinary;
  ErrorApiResponse _error;
  ServiceVeterinaryApiService();

  Future<ApiResponse> getAllVeterinaryByName(
      String name, String accessToken) async {
    var apiResponse = ApiResponse(statusResponse: 0);
    var queryParameters = {
      'name': name,
    };
    var url = Uri.http(Constants.urlAuthority,
        Constants.urlgetAllServiceByNameService, queryParameters);
    var resp = await http.get(url,
        headers: {HttpHeaders.authorizationHeader: 'Bearer' + accessToken});
    var respBody = json.decode(resp.body);
    apiResponse.statusResponse = resp.statusCode;

    if (apiResponse.statusResponse == 200) {
      _serviceVeterinary = ServiceVeterinary.fromJson(respBody);
      apiResponse.payload = _serviceVeterinary;
    } else {
      _error = ErrorApiResponse.fromJson(respBody);
      apiResponse.payload = _error;
    }
    return apiResponse;
  }
}
