import 'dart:convert';
import 'dart:io';

import 'package:clinical_veterinary/src/model/api_response_model.dart';
import 'package:clinical_veterinary/src/model/error_api_response_model.dart';
import 'package:clinical_veterinary/src/model/veterinary_model.dart';
import 'package:clinical_veterinary/src/utils/Constants.dart';
import 'package:http/http.dart' as http;

class VeterinaryApiService {
  Veterinary _veterinary;
  ErrorApiResponse _error;
  VeterinaryApiService();

  Future<ApiResponse> createVeterinary(
      Veterinary veterinary, String accessToken) async {
    var apiResponse = ApiResponse(statusResponse: 0);
    var body = json.encode(veterinary.toJsonRegistry());
    var uri = Uri.http(Constants.urlAuthority, Constants.urlcreateVeterinary);
    var res = await http.post(uri,
        headers: {
          HttpHeaders.contentTypeHeader: 'application/json',
          HttpHeaders.authorizationHeader: 'Bearer ' + accessToken
        },
        body: body);

    var resBody = json.decode(res.body);
    apiResponse.statusResponse = res.statusCode;
    if (apiResponse.statusResponse == 200) {
      _veterinary = Veterinary.fromJson(resBody);
      apiResponse.payload = _veterinary;
    } else {
      _error = ErrorApiResponse.fromJson(resBody);
      apiResponse.payload = _error;
    }
    return apiResponse;
  }

  Future<ApiResponse> updateVeterinary(
      Veterinary veterinary, String accessToken) async {
    var apiResponse = ApiResponse(statusResponse: 0);
    var body = json.encode(veterinary.toJson());
    var uri = Uri.http(Constants.urlAuthority, Constants.urlupdateVeterinary);
    var res = await http.put(uri,
        headers: {
          HttpHeaders.contentTypeHeader: 'application/json',
          HttpHeaders.authorizationHeader: 'Bearer ' + accessToken
        },
        body: body);

    var resBody = json.decode(res.body);
    apiResponse.statusResponse = res.statusCode;
    if (apiResponse.statusResponse == 200) {
      _veterinary = Veterinary.fromJson(resBody);
      apiResponse.payload = _veterinary;
    } else {
      _error = ErrorApiResponse.fromJson(resBody);
      apiResponse.payload = _error;
    }
    return apiResponse;
  }

  Future<ApiResponse> getAllVeterinary(String accessToken) async {
    List<Veterinary> listVeterinary;
    listVeterinary = [];
    var apiResponse = ApiResponse(statusResponse: 0);
    var url = Uri.http(Constants.urlAuthority, Constants.urlgetAllVeterinary);
    var res = await http.get(url,
        headers: {HttpHeaders.authorizationHeader: 'Bearer ' + accessToken});
    var resBody = json.decode(res.body);

    apiResponse.statusResponse = res.statusCode;

    listVeterinary = [];
    if (apiResponse.statusResponse == 200) {
      resBody.forEach((veterinary) {
        listVeterinary.add(veterinary.fromJson(veterinary));
        return veterinary;
      });
      apiResponse.payload = listVeterinary;
    } else {
      _error = ErrorApiResponse.fromJson(resBody);
      apiResponse.payload = _error;
    }

    return apiResponse;
  }

  Future<ApiResponse> getAllVeterinaryByName(
      String name, String accessToken) async {
    var apiResponse = ApiResponse(statusResponse: 0);
    var queryParameters = {
      'name': name,
    };
    var uri = Uri.http(Constants.urlAuthority,
        Constants.urlgetAllVeterinaryByName, queryParameters);
    var res = await http.get(uri,
        headers: {HttpHeaders.authorizationHeader: 'Bearer ' + accessToken});

    var resBody = json.decode(res.body);
    apiResponse.statusResponse = res.statusCode;
    if (apiResponse.statusResponse == 200) {
      _veterinary = Veterinary.fromJson(resBody);
      apiResponse.payload = _veterinary;
    } else {
      _error = ErrorApiResponse.fromJson(resBody);
      apiResponse.payload = _error;
    }

    return apiResponse;
  }

  Future<ApiResponse> deleteVeterinary(int id, String accessToken) async {
    var apiResponse = ApiResponse(statusResponse: 0);
    var queryParameters = {
      'id': id.toString(),
    };
    var uri = Uri.http(
        Constants.urlAuthority, Constants.urldeleteVeterinary, queryParameters);
    var res = await http.delete(uri,
        headers: {HttpHeaders.authorizationHeader: 'Bearer ' + accessToken});

    var resBody = json.decode(res.body);
    apiResponse.statusResponse = res.statusCode;
    if (apiResponse.statusResponse == 200) {
      _veterinary = Veterinary.fromJson(resBody);
      apiResponse.payload = _veterinary;
    } else {
      _error = ErrorApiResponse.fromJson(resBody);
      apiResponse.payload = _error;
    }

    return apiResponse;
  }
}
