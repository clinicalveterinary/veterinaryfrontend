class Constants {
  static const String urlAuthority =
      'ec2-18-221-218-155.us-east-2.compute.amazonaws.com:8083';
  static const String pathBase = '/api/veterinary';
  static const String contentTypeHeader = 'application/json';
  static const String authorizationHeader = 'Bearer ';
  static const String urlMedicalExamPDFEmail =
      pathBase + '/medicalexam/pdf/email';
  static const String urlCategorySave = pathBase + '/category/save';
  static const String urlCategoryUpdate = pathBase + '/category/update';
  static const String urlCategoryGetByName = pathBase + '/category/get/name';
  static const String urlIncomeGetAll = pathBase + '/income/allIncome';
  static const String urlgetAllEgress = pathBase + '/egress/all';
  static const String urlgetAllEgressByType = pathBase + '/egress/all/type';
  static const String urlgetAllEgressByPet = pathBase + '/egress/all/pet';
  static const String urlcreateEgress = pathBase + '/egress/save';
  static const String urlupdateEgress = pathBase + '/egress/update';
  static const String urlcreateClinicalHistory =
      pathBase + '/clinicalhistory/save';
  static const String urlcreateBranchOffice = pathBase + '/branchOffice/save';
  static const String urlupdateBranchOffice = pathBase + '/branchOffice/update';
  static const String urlgetAllBranchOffice = pathBase + '/branchOffice/all';
  static const String urldeleteBranchOffice =
      pathBase + '/branchOffice/delete/id';
  static const String urlgetAllPetByUser = pathBase + '/pet/pets/user';
  static const String urlcreateVeterinary = pathBase + '/veterinary/save';
  static const String urlupdateVeterinary = pathBase + '/veterinary/update';
  static const String urlgetAllVeterinary = pathBase + '/veterinary/all';
  static const String urlgetAllVeterinaryByName =
      pathBase + '/veterinary/all/name';
  static const String urldeleteVeterinary = pathBase + '/veterinary/delete/id';
  static const String urlcreateClinicalhistorydetail =
      pathBase + '/clinicalhistorydetail/save';
  //Api Invoice
  static const String urlfindAllByIdUser = pathBase + '/invoice/all/idUser';
  static const String urlfindAllByIdPet = pathBase + '/invoice/all/idPet';
  static const String urlgetOneById = pathBase + '/invoice/all/idPet';
  static const String urlinvoiceUpate = pathBase + '/invoice/update';
  static const String urlsendEmailByIdIvnoice =
      pathBase + '/sendEmailByIdIvnoice/pdf/email';
  //Api DetailInvoice
  static const String urlfindAllByIdInvoice =
      pathBase + '/invoice/all/idInvoice';

  static const String urlgetOneByIdPet = pathBase + '/cardvaccine/consul';
  static const String urlgetCardVaccine = pathBase + '/cardvaccine/info';
  static const String urlsendMailPDF = pathBase + '/cardvaccine/pdf/email';
  static const String urlgetAllByIdCardVaccine =
      pathBase + '/detailvaccine/all/cardvaccine';
  static const String urlgetAllByTopicAppointment =
      pathBase + '/appointment/all/topic/vaccine';
  //Api ServiceVeterinary
  static const String urlgetAllServiceByNameService =
      pathBase + '/service/all/name';

  static const String insertSuccess = 'registro exitoso';
  static const String updateSuccess = 'actualización exitoso';
}
